import json

with open('fix.json') as f:
    data = json.load(f)

with open('values.json') as f:
    values = json.load(f)

with open("outputfinal.txt","r") as file:
    lines = file.readlines()

output = open("output.txt","w")
for a in lines:
    date_stamp = a[0:24]
    a = a[24:].replace("||","|")
    b=a.split('|')
    

    if b[-1]:
        b = b[:-1]

    finalString = ""
    finalString+=date_stamp
    err = ""
    for elem in b[:-1]:
        if elem.__contains__('='):
            fixIn,valueIn = elem.split('=')
            value = valueIn
            if fixIn in values:
                value = values[fixIn][valueIn]
            if fixIn in data:
                finalString += data[fixIn]+'='+value+'|'
        else:
            err += elem + " "
    if b[-1].__contains__('='):
        fixIn,valueIn = b[-1].split('=')
        if fixIn in values:
            value = values[fixIn][valueIn]
        if fixIn in data:
            finalString += data[fixIn]+'='+value+'\n'
    else:
        err += elem+" "
        finalString += 'erreur='+err+'\n'
    #d = json.loads(finalString)
    output.write(finalString)

    #print(finalString)
# output.write("}")
    #esclient = Elasticsearch(['localhost:9200'])
    #esclient.index(index="in", doc_type="fix_message", body=finalString)
    #esclient.get(index="in", doc_type="fix_message", id=1)